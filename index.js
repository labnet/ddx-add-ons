'use strict';

// Iframe resizing
require('iframe-resizer').iframeResizerContentWindow;
var Bellhop = require('bellhop-iframe');

function ThirdParty() {
    var self = this;
    this.post = new window.Bellhop();
    this.post.connect();

    this.post.fetch('init', function(event) {
        self.post.origin = event.data;
    });
}

ThirdParty.prototype.send = function (event, data) {
    this.post.send(event, data);
}

ThirdParty.prototype.fetch = function (event, callback) {
    this.post.send(event, callback);
}

ThirdParty.prototype.on = function (event, callback) {
    this.post.on(event, callback);
}

ThirdParty.prototype.destroy = function () {
    this.post.destroy();
}

ThirdParty.prototype.disconnect = function () {
    this.post.disconnect();
}

ThirdParty.prototype.destroy = function () {
    this.post.destroy();
}

window.ThirdParty = ThirdParty;