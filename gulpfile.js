const gulp = require('gulp');
const browserify = require('browserify');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');

// Without watchify
gulp.task('browserify', () => {
    let bundler = browserify('./index.js', { debug: false });
    return bundleJS(bundler)
});

// Bundler for JS
function bundleJS(bundler) {
    return bundler.bundle()
        .on('error', (err) => {
            throw err
        })
        .pipe(source('index.js'))
        .pipe(buffer())
        .pipe(gulp.dest('dist'));
};

gulp.task('default', ['browserify']);